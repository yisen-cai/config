package top.yisen614.common.entity

import java.io.Serializable

class People : Serializable {
    // 人员编号
    var id: Int = 0
    // 姓名
    var name: String? = null

    override fun toString(): String {
        return "People{" +
                "id=" + id +
                ", name='" + name + '\''.toString() +
                '}'.toString()
    }

    companion object {
        const val serialVersionUID = 1415852192397895853L
    }
}
