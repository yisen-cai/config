package top.yisen614.common.service

import top.yisen614.common.entity.People

interface PeopleService {
    fun getPeople(people: People): People
}