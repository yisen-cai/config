# config

#### Description
 Spring Boot Centralized Configuration
Using Spring Config Server and Client
https://spring.io/guides/gs/centralized-configuration/
https://cloud.spring.io/spring-cloud-config/reference/html/#_security_2

#### Software Architecture
Use Maven to manage dependency, Root project use spring-cloud-dependencies manage package version, project could divided
to two module, server and client, sever module start before client, providing settings for client module, client load settings
from server, and could refresh settings if server has update settings.

#### Instructions

1. open in idea
2. run server
3. run client

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request