# config

#### 介绍
 Spring Boot配置中心
Using Spring Config Server and Client
https://spring.io/guides/gs/centralized-configuration/
https://cloud.spring.io/spring-cloud-config/reference/html/#_security_2

#### 软件架构
使用Maven管理依赖, 根项目使用spring-cloud-dependencies来管理依赖包版本, 项目分为server和client两个子模块,
其中server端先于client启动, 提供client的运行配置, client端读取server端配置运行项目, 并且可以根据server配置进行动态刷新

#### 使用说明

1. idea打开项目
2. 启动server
3. 启动client

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request