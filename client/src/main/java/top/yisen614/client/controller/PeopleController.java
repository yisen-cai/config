package top.yisen614.client.controller;

import top.yisen614.common.entity.People;
import top.yisen614.common.service.PeopleService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PeopleController {
    @Reference(version = "1.0.0")
    private PeopleService peopleService;


    @RequestMapping("/people/{name}")
    public People getPeople(@PathVariable("name") String name) {
        People people = new People();
        people.setName(name);
        return peopleService.getPeople(people);
    }
}
