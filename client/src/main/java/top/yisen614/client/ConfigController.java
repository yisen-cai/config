package top.yisen614.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * created by yisen614
 * 2019-08-07
 */
@RefreshScope
@RestController
public class ConfigController {
    @Value("${message:Hello default}")
    private String message;


    @RequestMapping("/message")
    String getMessage() {
        return this.message;
    }
}
