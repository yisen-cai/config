package top.yisen614.provider.service

import top.yisen614.common.entity.People
import top.yisen614.common.service.PeopleService
import org.apache.dubbo.config.annotation.Service

@Service(version = "1.0.0")
open class PeopleServiceImpl : PeopleService {
    override fun getPeople(people: People): People {
        return people
    }
}